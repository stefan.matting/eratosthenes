#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <vector>

using namespace std;

int read_Int(){
    int zahl;
    scanf("%d",&zahl);
    return zahl;
    }

// sucht primzahlen und vielfache
int prim(vector<bool> & feld, int M) {
    
    // 1 ist keine primzahl
    feld[ 1 - 1 ] = false;

    // zählt jede streichung, auch mehrfachstreichung
    int streich = 0;

    int i;
    for ( i = 2;  i*i < M; i++ ) {
        // ist i eine primzahl?
        if ( feld[ i - 1 ] ) {
            // dann streiche alle vielfachen von i
            int j;
            for ( j = 2; i * j <= M; j++ ) {
                feld[ ( i * j ) - 1 ] = false;
                streich++;
            }
        }
    }

    printf("Streichungen: %d\n", streich);
    return 0;
}

// zählt die anzahl der primzahlen in "feld"
int count( vector<bool> & feld, int M ) {
    int prime = 0;

    int i;
    for ( i = 1; i <= M; ++i ) {
        if ( feld[ i - 1 ] ) {
            prime++;
        printf("%d,", i);
        }
    }
    printf("\n");

    return prime;
}

int main(int argc, const char * argv[]){

    if ( argc < 2 ) {
        printf( "Bitte größe des Siebs als erstes argument\n" );
        return 1;
    }

    int M = atoi( argv[1] );
    vector<bool> feld(M);

    //alle elemente auf true=Primzahl
    int i;
    for ( i = 1; i <= M; i++ ) {
        feld[ i - 1 ] = true;
    } 
    
    prim(feld, M);

    int n = count(feld, M);
    printf("Primzahlen: %d\n", n);
    return 0;
}
